/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2019-03-05     whj4674672   first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include <drv_lcd.h>

#include "MainTask.h"
#include "bsp_tft_st7789.h"
#include "bsp_tft_lcd.h"
/* defined the LED0 pin: PB1 */
#define LED0_PIN    GET_PIN(B, 10)
extern void LCD_Task(void);
void emwin_thread_entry(void *parameter)
{
    MainTask();
}

void lcd_thread_entry(void *parameter)
{
    LCD_InitHard();
    while(1)
    {
        rt_thread_delay(50);
        LCD_Task();
    }
}
#define LCD_PWR_PIN    GET_PIN(B, 0)//����
int main(void)
{
    rt_thread_t tid,tid1;
    
    int count = 1;
    /* set LED0 pin mode to output */
    rt_pin_mode(LED0_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LCD_PWR_PIN, PIN_MODE_OUTPUT);
    rt_pin_write(LCD_PWR_PIN, PIN_HIGH);

    __HAL_RCC_CRC_CLK_ENABLE();

    tid1 = rt_thread_create("lcd", lcd_thread_entry, RT_NULL,
                     4096*2, 1, 10);
    rt_thread_startup(tid1);
    
    rt_thread_delay(50);
    
    tid = rt_thread_create("emwin", emwin_thread_entry, RT_NULL,
                     4096, 20, 10);
    rt_thread_startup(tid);
    
    while (count++)
    {
        rt_pin_write(LED0_PIN, PIN_HIGH);
        rt_thread_mdelay(500);
        rt_pin_write(LED0_PIN, PIN_LOW);
        rt_thread_mdelay(500);
    }
    return RT_EOK;
}
